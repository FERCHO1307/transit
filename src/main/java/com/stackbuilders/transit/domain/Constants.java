package com.stackbuilders.transit.domain;

import java.time.LocalTime;

public class Constants {

    public final static LocalTime RESTRICTION_START_TIME_MORNING = LocalTime.of(7, 00);
    public final static LocalTime RESTRICTION_END_TIME_MORNING = LocalTime.of(9, 30);
    public final static LocalTime RESTRICTION_START_TIME_NIGHT = LocalTime.of(16, 00);
    public final static LocalTime RESTRICTION_END_TIME_NIGHT = LocalTime.of(19, 30);
}
