package com.stackbuilders.transit.domain;

import com.stackbuilders.transit.domain.enums.DaysEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name = "rule")
public class Rule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "day_of_week", nullable = false)
    private DaysEnum dayOfWeek;

    @NotNull
    @Column(name = "last_plate_number", nullable = false)
    @Min(value = 0)
    @Max(value = 9)
    private Integer lastPlateNumber;

}
