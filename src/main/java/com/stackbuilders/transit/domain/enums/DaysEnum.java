package com.stackbuilders.transit.domain.enums;

public enum DaysEnum {
    DAY_MONDAY("Lunes"),
    DAY_TUESDAY("Martes"),
    DAY_WEDNESDAY("Miercoles"),
    DAY_THURSDAY("Jueves"),
    DAY_FRIDAY("Viernes"),
    DAY_SATURDAY("Sábado"),
    DAY_SUNDAY("Domingo");

    public final String label;

    DaysEnum(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }
}
