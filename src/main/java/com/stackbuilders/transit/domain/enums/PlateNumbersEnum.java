package com.stackbuilders.transit.domain.enums;

public enum PlateNumbersEnum {
    NUMBER_ZERO("Cero (0)"),
    NUMBER_ONE("Uno (1)"),
    NUMBER_TWO("Dos (2)"),
    NUMBER_THREE("Tres (3)"),
    NUMBER_FOUR("Cuatro (4)"),
    NUMBER_FIVE("Cinco (5)"),
    NUMBER_SIX("Seis (6)"),
    NUMBER_SEVEN("Siete (7)"),
    NUMBER_EIGHT("Ocho (8)"),
    NUMBER_NINE("Nueve (9)");

    public final String label;

    PlateNumbersEnum(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }
}
