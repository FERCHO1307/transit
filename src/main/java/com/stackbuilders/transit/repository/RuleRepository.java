package com.stackbuilders.transit.repository;

import com.stackbuilders.transit.domain.Rule;
import com.stackbuilders.transit.domain.enums.DaysEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@Repository
public interface RuleRepository extends JpaRepository<Rule, Long> {

    List<Rule> findByDayOfWeek(DaysEnum dayEnum);
}
