package com.stackbuilders.transit.rest;

import com.stackbuilders.transit.service.ConstantService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/constants")
public class ConstantController {

    private final ConstantService constantService;

    public ConstantController(ConstantService constantService) {
        this.constantService = constantService;
    }

    @GetMapping("/days")
    public ResponseEntity<List<String>> getAllDays() {
        List<String> daysEnumList = constantService.findAllDays();
        return ResponseEntity.ok().body(daysEnumList);
    }

}
