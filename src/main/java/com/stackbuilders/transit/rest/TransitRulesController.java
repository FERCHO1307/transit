package com.stackbuilders.transit.rest;

import com.stackbuilders.transit.rest.dto.ResponseDto;
import com.stackbuilders.transit.rest.dto.RuleDto;
import com.stackbuilders.transit.service.TransitRulesService;
import com.stackbuilders.transit.service.exceptions.PlateValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/transit")
public class TransitRulesController {

    private final TransitRulesService transitRulesService;

    public TransitRulesController(TransitRulesService transitRulesService) {
        this.transitRulesService = transitRulesService;
    }

    @PostMapping("/validate-plate")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ResponseDto> validatePlate(@RequestBody RuleDto rule) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto = transitRulesService.validatePlateStructure(rule.getPlate(), rule.getLocalTime(), rule.getDaysEnum());
        } catch (PlateValidationException plateValidationException) {
            responseDto.setResult(Boolean.FALSE);
            responseDto.setMessage(plateValidationException.getMessage());
        }

        return new ResponseEntity<ResponseDto>(responseDto, null, HttpStatus.OK);
    }
}
