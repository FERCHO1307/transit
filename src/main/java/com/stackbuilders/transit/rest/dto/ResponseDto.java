package com.stackbuilders.transit.rest.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseDto {

    private Boolean result;
    private String message;
}
