package com.stackbuilders.transit.rest.dto;

import com.stackbuilders.transit.domain.enums.DaysEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleDto {

    private String plate;
    private LocalTime localTime;
    private DaysEnum daysEnum;
}
