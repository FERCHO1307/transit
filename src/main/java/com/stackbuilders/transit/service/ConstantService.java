package com.stackbuilders.transit.service;


import com.stackbuilders.transit.domain.enums.DaysEnum;
import com.stackbuilders.transit.domain.enums.PlateNumbersEnum;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ConstantService {

    /**
     * Return List of DaysEnum
     *
     * @return
     */
    public List<String> findAllDays() {
        return Stream.of(DaysEnum.values())
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

}
