package com.stackbuilders.transit.service;

import com.stackbuilders.transit.domain.Constants;
import com.stackbuilders.transit.domain.Rule;
import com.stackbuilders.transit.domain.enums.DaysEnum;
import com.stackbuilders.transit.repository.RuleRepository;
import com.stackbuilders.transit.rest.dto.ResponseDto;
import com.stackbuilders.transit.service.exceptions.PlateValidationException;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TransitRulesService {

    private RuleRepository ruleRepository;

    public TransitRulesService(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    public ResponseDto validatePlateStructure(String plate, LocalTime localTime, DaysEnum daysEnum) throws PlateValidationException {
        ResponseDto responseDto = new ResponseDto();
        Integer lastDigitPlate = validPlate(plate);
        Boolean result = Boolean.FALSE;
        if (isAllowCirculateByPlateAndDay(lastDigitPlate, daysEnum)) {
            if (isAllowCirculateByTime(localTime)) {
                responseDto.setResult(Boolean.TRUE);
                responseDto.setMessage("Usted SI puede circular");
            }
        } else {
            responseDto.setResult(Boolean.FALSE);
            responseDto.setMessage("Usted No puede circular");
        }
        return responseDto;
    }

    public Boolean isAllowCirculateByTime(LocalTime localTime) {
        return (localTime.isBefore(Constants.RESTRICTION_START_TIME_MORNING) || localTime.isAfter(Constants.RESTRICTION_END_TIME_MORNING)) &&
                (localTime.isBefore(Constants.RESTRICTION_START_TIME_NIGHT) || localTime.isAfter(Constants.RESTRICTION_END_TIME_NIGHT));
    }

    public Boolean isAllowCirculateByPlateAndDay(Integer lastNumberPlate, DaysEnum daysEnum) {
        List<Rule> ruleList = ruleRepository.findByDayOfWeek(daysEnum);
        Rule ruleFind = ruleList.stream()
                .filter(rule -> lastNumberPlate.equals(rule.getLastPlateNumber()))
                .findAny()
                .orElse(null);
        return ruleFind == null ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * Validate expression regular plate
     *
     * @param plate
     * @return
     */
    public Integer validPlate(String plate) throws PlateValidationException {

        //Validate regular expression
        plate = plate.replace("-", "");
        Pattern pat = Pattern.compile("^[A-Z]{3}\\d{3,4}");
        Matcher mat = pat.matcher(plate);

        if (mat.matches()) {
            String sectionLetters = plate.substring(0, 3);
            String sectionNumbers = plate.substring(3, plate.length() == 6 ? 6 : 7);

            if (validateFirstLetter(sectionLetters)) {
                return Character.getNumericValue(sectionNumbers.charAt(sectionNumbers.length() - 1));
            } else {
                throw new PlateValidationException("La placa ingresada comienza con una letra errónea");
            }
        } else {
            throw new PlateValidationException("El formato de la placa ingresa es incorrecto");
        }
    }

    /**
     * Validate letters that do not belong to any province
     *
     * @param sectionLetters
     * @return
     */
    public Boolean validateFirstLetter(String sectionLetters) {
        List<String> listNotValidPlates = Stream.of("D", "F")
                .collect(Collectors.toList());
        return !listNotValidPlates.contains(sectionLetters.substring(0, 1));
    }
}
