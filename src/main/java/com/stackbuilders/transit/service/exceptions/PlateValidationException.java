package com.stackbuilders.transit.service.exceptions;

public class PlateValidationException extends Exception {

    public PlateValidationException(String message) {
        super(message);
    }
}

