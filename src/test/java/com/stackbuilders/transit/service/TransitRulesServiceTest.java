package com.stackbuilders.transit.service;

import com.stackbuilders.transit.TransitApplication;
import com.stackbuilders.transit.domain.enums.DaysEnum;
import com.stackbuilders.transit.service.exceptions.PlateValidationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TransitApplication.class)
public class TransitRulesServiceTest {

    @Autowired
    private TransitRulesService ruleService;

    @Test
    public void testRangeRestrictionMorning() {
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(7, 00))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(7, 01))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(6, 59))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(8, 00))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(3, 23))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(10, 05))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(9, 20))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(13, 00))).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testRangeRestrictionNight() {
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(15, 59))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(16, 01))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(18, 00))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(19, 23))).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(20, 23))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(22, 05))).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByTime(LocalTime.of(18, 50))).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testRangeRestricionByPlateInMonday() {
        assertThat(ruleService.isAllowCirculateByPlateAndDay(0, DaysEnum.DAY_MONDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(5, DaysEnum.DAY_MONDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(1, DaysEnum.DAY_MONDAY)).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(2, DaysEnum.DAY_MONDAY)).isEqualTo(Boolean.FALSE);
    }

    @Test
    public void testRangeRestricionByPlateWorkDays() {
        assertThat(ruleService.isAllowCirculateByPlateAndDay(3, DaysEnum.DAY_TUESDAY)).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(5, DaysEnum.DAY_TUESDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(5, DaysEnum.DAY_WEDNESDAY)).isEqualTo(Boolean.FALSE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(8, DaysEnum.DAY_TUESDAY)).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testRangeRestricionByPlateWeekend() {
        assertThat(ruleService.isAllowCirculateByPlateAndDay(0, DaysEnum.DAY_SUNDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(2, DaysEnum.DAY_SUNDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(4, DaysEnum.DAY_SUNDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(3, DaysEnum.DAY_SUNDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(8, DaysEnum.DAY_SATURDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(9, DaysEnum.DAY_SATURDAY)).isEqualTo(Boolean.TRUE);
        assertThat(ruleService.isAllowCirculateByPlateAndDay(6, DaysEnum.DAY_SATURDAY)).isEqualTo(Boolean.TRUE);
    }

    @Test()
    public void validatePlateFormat() {
        try {
            assertThat(ruleService.validPlate("ABC7159")).isEqualTo(9);
            assertThat(ruleService.validPlate("#$%#@!#@!")).isInstanceOf(PlateValidationException.class);
            assertThat(ruleService.validPlate("ABC-7159")).isEqualTo(9);
            assertThat(ruleService.validPlate("ABC-715")).isEqualTo(5);
            assertThat(ruleService.validPlate("DBC-715")).isInstanceOf(PlateValidationException.class);
            assertThat(ruleService.validPlate("FBC-715")).isInstanceOf(PlateValidationException.class);
            assertThat(ruleService.validPlate("IBB-7169")).isEqualTo(9);
            assertThat(ruleService.validPlate("PCX8916")).isEqualTo(6);
        } catch (PlateValidationException plateValidationException) {
            System.out.println(plateValidationException.getMessage());
        }
    }

}
